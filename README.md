# comb
comb through files and update ID3 tags, Vorbis comments, whatever.

# components

- [comb-cli](./comb/cli/README.md): a command-line tool for ingesting audio files
- [a, uh, media server, I guess](./comb/app/README.md): simple web app for browsing and playback
