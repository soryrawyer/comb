"""
the opposite of changes
"""

from typing import Dict


ALBUM = "album"
TRACK_NAME = "track_name"
ARTIST = "artist"
RELEASE = "release"
TRACK = "track"
TRACK_NUMBER = "track_number"

ARTIST_ID = "artist_id"
RELEASE_ID = "release_id"
TRACK_ID = "track_id"
DISCOGS_ID = "discogs_id"
DISCOGS_EXTRA = "discogs_extra"
ID = "id"
NAME = "name"
POSITION = "position"

AUDIO_META_BY_TYPE: Dict[str, Dict[str, str]] = {
    "mp3": {TRACK_NAME: "TIT2", ARTIST: "TPE1", TRACK_NUMBER: "TRCK", ALBUM: "TALB"},
    "flac": {
        TRACK_NAME: "title",
        ARTIST: "artist",
        TRACK_NUMBER: "tracknumber",
        ALBUM: "album",
    },
}
