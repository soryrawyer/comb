"""
cli part of the application
"""

import argparse
import logging
import sys

from .commands.config import ConfigCommand
from .commands.download import Download
from .commands.ingest import Ingest

# a list of available commands
# this should be a map of command name to command class
_COMMANDS = {
    "ingest": Ingest,
    "download": Download,
    "config": ConfigCommand,
}


logger = logging.getLogger(__name__)


def set_up_logger(log_level):
    """
    add some options to our logger
    """
    handler = logging.StreamHandler(stream=sys.stdout)
    log_format = "%(levelname)s:%(module)s:%(filename)s:%(lineno)d %(message)s"
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.propagate = False
    if log_level:
        logger.setLevel(log_level)


def _parse_args():
    """
    parse arguments! get commands to add their own subparsers!
    """
    parser = argparse.ArgumentParser(__doc__)
    levels = ("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL")
    parser.add_argument("--log-level", choices=levels)
    subparsers = parser.add_subparsers(dest="command")
    for command in _COMMANDS.values():
        command.add_subparser(subparsers)
    return parser.parse_args()


def main():
    """
    kick off whatever it is we're doing here
    """
    args = _parse_args()
    set_up_logger(args.log_level)
    _COMMANDS[args.command].run(args)


if __name__ == "__main__":
    main()
