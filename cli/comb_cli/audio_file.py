"""
a wrapper around audio files
"""

from pathlib import Path

import mutagen

from comb_cli import constants as c


FIELD_MAPPING = {
    c.ARTIST: {c.NAME: "artist"},
    c.RELEASE: {c.NAME: "release"},
    c.TRACK: {c.NAME: "track_name", c.POSITION: "position"},
}


class AudioFile:
    """
    wrapper around an audio file, abstracting away idiosyncracies like
    in metadata field naming
    """

    def __init__(self, path: Path):
        self.path = path
        self._file = mutagen.File(path)
        if isinstance(self._file, mutagen.flac.FLAC):
            self.metadata_fields = c.AUDIO_META_BY_TYPE["flac"]
            self.field_accessors = lambda key: self._file.get(self.metadata_fields[key])
        elif isinstance(self._file, (mutagen.mp3.MP3, mutagen.mp3.EasyMP3)):
            self.metadata_fields = c.AUDIO_META_BY_TYPE["mp3"]
            self.field_accessors = lambda key: self._file.get(
                self.metadata_fields[key]
            ).text

    def __repr__(self):
        _p = f"path={self.path}"
        _a = f"artist={self.artist}"
        _r = f"release={self.release}"
        _t = f"track={self.track_name}"
        return f"AudioFile({_p}, {_a}, {_r}, {_t})"

    def _get_property(self, key):
        """
        look up the metadata key in the mutagen object
        """
        return self.field_accessors(key)

    @property
    def artist(self):
        """
        return the artist listed in the metadata of this file
        """
        return self._get_property(c.ARTIST)[0]

    @property
    def release(self):
        """
        return the release listed in the metadata of this file
        """
        return self._get_property(c.ALBUM)[0]

    @property
    def track_name(self):
        """
        return the track name listed in the metadata of this file
        """
        return self._get_property(c.TRACK_NAME)[0]

    @property
    def position(self):
        """
        return the track position listed in the metadata of this file
        """
        value = self._get_property(c.TRACK_NUMBER)[0]
        if value is None:
            return None

        return value.split("/")[0]

    def save_metadata(self):
        """
        save new information to the file
        """
        self._file.save()
