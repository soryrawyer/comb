"""
small api client for comb
"""

import os
import urllib.parse

from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List

import requests

from .config import Config


ENV_BASE_URLS = {
    "prd": "https://comb.sory.biz",
    "local": "http://localhost:5000",
}


@dataclass
class Artist:
    """
    a container from and into which we can marshal and unmarshal artist json
    """

    name: str
    artist_id: int

    @classmethod
    def from_json(cls, blob: Dict):
        return cls(blob["name"], blob["artist_id"])

    def to_json(self) -> Dict:
        return {"name": self.name, "artist_id": self.artist_id}


@dataclass
class Release:
    """
    a container from and into which we can marshal and unmarshal Release json
    """

    name: str
    release_id: int

    @classmethod
    def from_json(cls, blob: Dict):
        return cls(blob["name"], blob["release_id"])

    def to_json(self) -> Dict:
        return {"name": self.name, "release_id": self.release_id}


@dataclass
class Track:
    """
    a container from and into which we can marshal and unmarshal Track json
    """

    name: str
    track_id: int

    @classmethod
    def from_json(cls, blob: Dict):
        return cls(blob["name"], blob["track_id"])

    def to_json(self) -> Dict:
        return {"name": self.name, "track_id": self.track_id}


@dataclass
class CombClient:
    """
    make api calls, return json, handle retries, all that
    """

    session: requests.Session
    email: str
    password: str
    base: str

    @classmethod
    def from_config(cls, config: Config = None, env="prd", authenticate=False):
        if not config:
            config = Config()
        session = requests.Session()
        session.headers.update({"Content-Type": "application/x-www-form-urlencoded"})
        instance = cls(
            session,
            config.expect("email"),
            config.expect("password"),
            ENV_BASE_URLS[env],
        )
        if authenticate:
            instance.authenticate()
        return instance

    def authenticate(self):
        auth = urllib.parse.urlencode({"email": self.email, "password": self.password})
        self.session.post(
            os.path.join(self.base, "auth"), data=auth, allow_redirects=False
        )

    def _get(self, url, entity=None, params: Dict = None):
        """
        base get class, which will unmarshal responses into the given entity
        """
        if params is None:
            params = {}
        resp = self.session.get(url, params=params)
        resp.raise_for_status()
        if entity is not None:
            return [entity.from_json(x) for x in resp.json()]
        return resp.json()

    def _post(self, url, entity=None, params: Dict = None):
        if params is None:
            params = {}
        resp = self.session.post(url, params=params)
        resp.raise_for_status()
        if entity is not None:
            # TODO: handle when json returns a list vs single object
            return entity.from_json(resp.json())
        return resp.json()

    def get_artists(self) -> List[Artist]:
        return self._get(os.path.join(self.base, "artists"), Artist)

    def get_releases(self, artist_id: int = None) -> List[Release]:
        params = {}
        if artist_id:
            params["artist_id"] = artist_id
        return self._get(os.path.join(self.base, "releases"), Release, params=params)

    def get_tracks(self, release_id: int = None) -> List[Track]:
        params = {}
        if release_id:
            params["release_id"] = release_id
        return self._get(os.path.join(self.base, "tracks"), Track, params=params)

    def get_playback_url(self, track_id: int) -> str:
        return self._get(os.path.join(self.base, "playback_url", str(track_id)))[
            "signed_url"
        ]

    def get_or_create_artist(self, name: str) -> Artist:
        params = {"name": name}
        return self._post(os.path.join(self.base, "artists"), Artist, params=params)

    def get_or_create_release(self, name: str, artist_id: int) -> Release:
        params = {"name": name, "artist_id": artist_id}
        return self._post(os.path.join(self.base, "releases"), Release, params=params)

    def get_or_create_track(self, name: str, release_id: int) -> Track:
        params = {"name": name, "release_id": release_id}
        return self._post(os.path.join(self.base, "tracks"), Track, params=params)

    def upload_file(self, track_id: int, path: Path):
        params = {"suffix": path.suffix}
        url = self._get(
            os.path.join(self.base, "tracks", str(track_id), "upload_url"),
            params=params,
        )["signed_url"]
        with open(path, "rb") as data:
            files = {path.name: data}
            self.session.put(url, files=files)
