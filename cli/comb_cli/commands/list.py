"""
list files, either here or remotely?
"""


from dataclasses import dataclass


DEFAULT_ATTRIBUTES = {"artist_name", "release_name", "track_name"}
AVAILABLE_ATTRIBUTES = {
    "artist_name",
    "release_name",
    "track_name",
    "position",
    "location",
}


@dataclass
class ListFiles:
    """
    comb-cli command to list files, either locally or remotely

    usage:
    list files by artist, release?
    list total files available

    printing: tabular? how could this interoperate with less/bat?
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("list", help="list files")
        parser.add_argument("--artist_name", help="filter results by artist ID")
        parser.add_argument("--release_name", help="filter results by release ID")
        parser.add_argument("--attributes", help="list of fields to return")
        return parser

    @staticmethod
    def run(args):
        """
        retrieve and print the list of attributes requested
        """
        ...
