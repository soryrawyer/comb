"""
cli commands for managing comb-cli configuration
"""

import pprint

from dataclasses import dataclass
from typing import Any


from ..config import Config


def set_config(config: Config, name: str, value: Any):
    """
    set a value for `name` in the given config
    """
    config.set(name, value)
    config.flush()


@dataclass
class ConfigCommand:
    """
    manage configuration values
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("config", help=__doc__)
        another_parser = parser.add_subparsers(dest="sub_command")
        # subparser for listing config contents. takes no additional arguments
        another_parser.add_parser("list", help="print configuration")

        set_parser = another_parser.add_parser(
            "set", help="set a value for a configuration option"
        )
        set_parser.add_argument("name", help="name of the configuration option")
        set_parser.add_argument("value", help="value of configuration option")

        return parser

    @staticmethod
    def run(args):
        config = Config()
        print(args)
        if args.sub_command == "list":
            pprint.pprint(config.items)
        elif args.sub_command == "set":
            set_config(config, args.name, args.value)
