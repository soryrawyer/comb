"""
a command to ingest files
"""

import argparse
import logging
import os

from dataclasses import dataclass
from functools import partial
from pathlib import Path

from ..client import CombClient
from ..config import Config

from ..audio_file import AudioFile


logger = logging.getLogger(__name__)


def _path(parser: argparse.ArgumentParser, path_str) -> Path:
    """
    accept a string of a path (directory or file) and
    return a pathlib.Path if it exists, or raise an exception
    if it doesn't
    """
    if not os.path.exists(path_str):
        parser.print_usage()
        raise ValueError("<path> must exist")
    return Path(path_str)


def ingest_track(filename: Path, client: CombClient):
    """
    add artist
    add release
    add track
    upload track


    ingest track by:
    - reading file into an AudioFile
    - creating: artist, release, track
    - getting an upload path for the track
    - posting the file to the given url
    """
    audio_file = AudioFile(filename)
    logger.info(audio_file)
    artist = client.get_or_create_artist(audio_file.artist)
    logger.debug(artist)
    release = client.get_or_create_release(audio_file.release, artist.artist_id)
    logger.debug(release)
    track = client.get_or_create_track(audio_file.track_name, release.release_id)
    logger.debug(track)
    client.upload_file(track.track_id, filename)
    print("...tada?")
    return


@dataclass
class Ingest:
    """
    a class fulfilling an informal "command" interface
    this class provides a method to add a subparser to specify what arguments
    can be passed.
    It also has an entrypoint to kick off the command.
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("ingest", help="ingest files")
        parser.add_argument("path", type=partial(_path, parser))
        parser.add_argument("--dry-run", action="store_true")
        parser.add_argument("--force", action="store_true")
        return parser

    @staticmethod
    def run(args):
        config = Config()
        client = CombClient.from_config(config)
        if os.path.isdir(args.path):
            # if the given path is a directory, walk it and
            # read any files that are passed into it
            # try to get some information from discogs
            # roughly speaking, I'm thinking the process looks like
            # os walk -> AudioFile -> info lookup -> Artist/Release/Track -> DB
            for (dirpath, _, filenames) in os.walk(args.path):
                # dirpath: str: directory path
                # dirnames: list of directories in the current folder
                # filenames: list of file name in os.path.join(dirpath, dirnames[i])
                for name in filenames:
                    logger.debug(f"reading: {name}")
                    ingest_track(Path(os.path.join(dirpath, name)), client)
        else:
            ingest_track(args.path, client)
