"""
Download files from comb server
"""

import logging
import os

from dataclasses import dataclass
from typing import Union

import requests

from ..config import Config
from ..client import CombClient


logger = logging.getLogger(__name__)


def get_track_path(
    base: str, artist: Union[int, str], release: Union[int, str], track: Union[int, str]
) -> str:
    """
    return the local path for where tracks should go
    """
    return os.path.join(
        base,
        str(artist).replace("/", "-"),
        str(release).replace("/", "-"),
        str(track).replace("/", "-"),
    )


@dataclass
class Download:
    """
    comb-cli download album "artist name" "album name"
    """

    @staticmethod
    def add_subparser(subparsers):
        parser = subparsers.add_parser("download", help="download some stuff!")
        parser.add_argument("artist_name", type=str)
        parser.add_argument("release_name", type=str)
        return parser

    @staticmethod
    def run(args):
        config = Config()
        client = CombClient.from_config(config, authenticate=True)
        # get artists, find albums by artist ID
        # get track ids for album ID
        # get playback urls for each track
        # TODO: write some kind of "get [artist|release] by name" api endpoint
        artists = client.get_artists()
        # TODO: replace this with levenstein distance
        logger.debug(artists)
        artist = [x for x in artists if x.name.lower() == args.artist_name][0]
        releases = client.get_releases(artist.artist_id)
        logger.debug(releases)
        release = [x for x in releases if x.name.lower() == args.release_name][0]
        tracks = client.get_tracks(release.release_id)
        logger.debug(tracks)
        playback_urls = [(t, client.get_playback_url(t.track_id)) for t in tracks]
        for track, url in playback_urls:
            # make sure local path exists
            # save url content to file
            path = get_track_path(
                config.get("local_landing_base"),
                str(artist.artist_id),
                str(release.release_id),
                str(track.track_id),
            )
            os.makedirs(os.path.dirname(path), exist_ok=True)
            r = requests.get(url, stream=True)
            with open(path, "wb") as audio:
                # so, this will write to a path with ids. maybe we could symlink
                # this to a path with names, so VLC can display human-readable
                # information?
                for chunk in r.iter_content(chunk_size=128):
                    audio.write(chunk)
            presentation_path = get_track_path(
                config.get("local_dest_base"), artist.name, release.name, track.name
            )
            os.makedirs(os.path.dirname(presentation_path), exist_ok=True)
            os.symlink(path, presentation_path)
            logger.info(f"wrote {presentation_path}")
