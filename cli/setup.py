"""
and so begins the great fiasco of python packaging
"""

from setuptools import setup, find_packages

setup(
    name="comb",
    version="0.0.1",
    description="command-line tool for comb application",
    packages=find_packages(),
    entry_points={"console_scripts": ["comb-cli = comb_cli.main:main"]},
    install_requires=[
        "requests",
    ],
)
