# comb-cli
ye olde command-line program

## configuration
"email" and "password" can be set using  
`comb-cli config set email "user@example.com"`  
and  
`comb-cli config set password "password"`  

## commands

### ingest
Point this at a directory and it will:

- walk through the files
- read the metadata
- make an attempt at enriching and standardizing this metadata using Discogs
- store this information in a database
- upload the file to GCS

### download
given an album name and artist name, the cli will download all tracks in that album

### config
can either list all current config options and their values, or can set new or existing options to new, exciting values