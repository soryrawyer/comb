CREATE TABLE IF NOT EXISTS artist_releases (
    artist_id int REFERENCES artists(id),
    release_id int REFERENCES releases(id),
    primary key (artist_id, release_id)
);
