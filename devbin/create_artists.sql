CREATE TABLE IF NOT EXISTS artists (
    id serial,
    name varchar(256),
    discogs_id varchar(32),
    discogs_extra json,
    PRIMARY KEY (id),
    constraint unique_artist_discogs_id UNIQUE(discogs_id)
);
