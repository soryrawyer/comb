CREATE TABLE IF NOT EXISTS releases (
    id serial,
    name varchar(256),
    discogs_id varchar(32),
    discogs_extra json,
    primary key (id),
    constraint unique_release_discogs_id UNIQUE(discogs_id)
);
