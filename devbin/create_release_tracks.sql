CREATE TABLE IF NOT EXISTS release_tracks (
    release_id int REFERENCES releases(id),
    track_id int REFERENCES tracks(id),
    primary key (release_id, track_id)
);
