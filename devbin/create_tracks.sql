CREATE TABLE IF NOT EXISTS tracks (
    id serial,
    name varchar(256),
    location varchar(64),
    discogs_extra json,
    position text,
    primary key (id)
);
