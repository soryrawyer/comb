"""
config.py : Arrakis. Dune. Desert planet.
"""

import json
import os

from pathlib import Path
from typing import Any, Optional

CONFIG_DIR = Path(os.environ.get("COMB_CONFIG", os.path.expanduser("~/.config/comb/")))


class Config:
    """
    Manage a configuration file
    """

    def __init__(self):
        if not os.path.isdir(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)
        self.config_dif = CONFIG_DIR
        self.basename = "config.json"
        self.path = os.path.join(self.config_dif, self.basename)
        if not os.path.exists(self.path):
            open(self.path, "a").close()
            self.items = {}
            return

        with open(self.path) as conf_raw:
            self.items = json.load(conf_raw)

    def expect(self, key: str) -> str:
        """
        like get, but raises an exception if there's no value
        """
        value = self.get(key)
        if value is None:
            raise ValueError(f"no value for key {key}")

        return value

    def get(self, key: str) -> Optional[str]:
        """
        return a value for the given key, None if the key does not exist
        """
        return os.environ.get(key, self.items.get(key))

    def set(self, key: str, value: Any):
        """
        update the config value for the given key
        """
        self.items[key] = value

    def flush(self):
        """
        flush the current config object to disk
        """
        with open(self.path, "w") as conf:
            json.dump(self.items, conf)
