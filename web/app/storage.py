"""
storage.py : interface with cloud storage
"""

import os

from dataclasses import dataclass, field
from datetime import timedelta
from pathlib import Path
from typing import Union

from google.cloud import storage

from .config import Config


def get_client() -> storage.Client:
    """
    return a google cloud storage client
    """
    return storage.Client()


@dataclass
class GcsStorage:
    """
    config-aware storage client
    """

    _client: storage.Client
    bucket_name: str
    media_base: Union[str, Path]
    bucket: storage.Bucket = field(init=False)

    def __post_init__(self):
        self.bucket = self._client.get_bucket(self.bucket_name)

    def upload_file(self, artist, track, path: Path):
        """
        upload the file at `path` to the given GCS prefix

        types are here and not in the definition because bringing in the
        models breaks the CLI

        :type artist: comb.model.Artist
        :type track: comb.model.Track
        """
        if not isinstance(path, Path):
            path = Path(path)

        blob_name = self.generate_track_location(
            str(artist.artist_id),
            str(track.release.release_id),
            f"{track.track_id}{path.suffix}",
        )
        blobject = storage.blob.Blob(blob_name, self.bucket)
        with open(path, "r+b") as audio:
            blobject.upload_from_file(audio)
        return blob_name

    def get_signed_url(self, track, expiration=timedelta(minutes=60)):
        """
        return a signed GCS url for the given track

        TODO: change the default expiration at some point

        :type track: Track
        """
        blob = storage.blob.Blob(track.location, self.bucket)
        return blob.generate_signed_url(expiration=expiration, method="GET")

    def get_signed_upload_url(
        self, track, suffix: str = "", expiration=timedelta(minutes=60)
    ):
        """
        return a signed upload url to upload a file for the given track
        or, maybe it should use the `create_resumable_upload_session` method instead?

        :type track: Track
        """
        blob = self.bucket.blob(
            self.generate_track_location(
                str(track.release.artist.artist_id),
                str(track.release.release_id),
                f"{track.track_id}{suffix}",
            )
        )
        return blob.create_resumable_upload_session()

    def generate_track_location(self, artist_id, release_id, track_file_name):
        return os.path.join(
            self.media_base,
            str(artist_id),
            str(release_id),
            track_file_name,
        )

    @classmethod
    def from_config(cls, config: Config = None):
        """
        create a new Storage based on the provided config
        """
        if config is None:
            config = Config()

        return cls(
            storage.Client(), config.expect("bucket"), Path(config.expect("media_base"))
        )
