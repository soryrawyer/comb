"""
route handlers relating to authentication
"""

from flask import Blueprint, flash, render_template, request, redirect, url_for, json
from flask_login import login_required, login_user, logout_user
from werkzeug.security import check_password_hash, generate_password_hash

from .db import dopg
from .models import User

auth = Blueprint("auth", __name__)


@auth.route("/login")
def login():
    return render_template("login.html")


# TODO: replace this with something like a "login-redir" route
# this probably would be more user-friendly for api clients
@auth.route("/login", methods=["POST"])
def login_post():
    email = request.form.get("email")
    password = request.form.get("password")
    remember = True if request.form.get("remember") else False

    user = User.get_by_email(email)
    if not user or not check_password_hash(user.password, password):
        flash(
            "look, either we couldn't find a user associated with your email, or the password was incorrect"
        )
        return redirect(url_for("auth.login"))

    login_user(user, remember=remember)

    return redirect(url_for("main.profile"))


@auth.route("/auth", methods=["POST"])
def auth_no_redir_post():
    """
    this is to authenticate without being redirected
    """
    email = request.form.get("email")
    password = request.form.get("password")
    remember = True if request.form.get("remember") else False

    user = User.get_by_email(email)
    if not user or not check_password_hash(user.password, password):
        data = {
            "msg": (
                "look, either we couldn't find a user associated with your "
                "email, or the password was incorrect"
            )
        }
        # TODO: there could probably be a better return status for this
        return json.jsonify(data), 400

    login_user(user, remember=remember)

    return json.jsonify({"msg": "success!"})


@auth.route("/signup")
def signup():
    return render_template("signup.html")


@auth.route("/signup", methods=["POST"])
def signup_post():
    email = request.form.get("email")
    password = request.form.get("password")
    name = request.form.get("name")

    user = User.get_by_email(email)
    if user:
        flash("a user with this email already exists!")
        return redirect(url_for("auth.login"))

    user = User(
        email=email.lower(),
        password=generate_password_hash(password, method="sha256"),
        name=name,
    )
    dopg.database.commit()

    return redirect(url_for("auth.login"))


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main.index"))
