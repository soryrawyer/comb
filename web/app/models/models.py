"""
just one file for models

it's worth noting that we can't use attrs here
it conflicts in some way with how PonyORM entities need to
inherit from the pony.orm.Entity class
"""

from datetime import datetime

from flask_login import UserMixin
from pony import orm

from app.db import dopg


def _name_matches(rec, name: str) -> bool:
    """
    simple utility to compare a given entity name to the name
    stored in the DB
    """
    return rec.name.lower() == name.lower().strip()


class Artist(dopg.database.Entity):
    """
    artist Entity for PonyORM
    """

    _table_ = "artists"

    artist_id = orm.PrimaryKey(int, auto=True, column="id")
    name = orm.Required(str)
    discogs_id = orm.Optional(int)
    releases = orm.Set("Release")
    discogs_extra = orm.Optional(orm.Json)

    def __repr__(self):
        aid = f"artist_id={self.artist_id}"
        n = f"name={self.name}"
        di = f"discogs_id={self.discogs_id}"
        return f"Artist({aid}, {n}, {di})"

    @staticmethod
    def get_by_name(name: str):
        return Artist.select(lambda a: _name_matches(a, name)).first()

    @staticmethod
    def get_one(entity_id: int):
        """
        get a single entity instance, return None if not found
        """
        try:
            return Artist[entity_id]
        except orm.ObjectNotFound:
            return None


class Release(dopg.database.Entity):
    """
    release Entity for PonyORM
    """

    _table_ = "releases"

    release_id = orm.PrimaryKey(int, auto=True, column="id")
    name = orm.Required(str)
    artist = orm.Required(Artist, column="artist_id")
    discogs_id = orm.Optional(int)
    tracks = orm.Set("Track", cascade_delete=False)
    discogs_extra = orm.Optional(orm.Json)

    def __repr__(self):
        rid = f"release_id={self.release_id}"
        n = f"name={self.name}"
        a = f"release={self.artist}"
        di = f"discogs_id={self.discogs_id}"
        return f"Release({rid}, {n}, {a}, {di})"

    @staticmethod
    def get_by_name(name: str, artist_id: int = None):
        """
        get a release by name and, possibly, artist name as well
        """
        query = Release.select(lambda r: _name_matches(r, name))
        if artist_id is not None:
            query = query.filter(lambda r: r.artist.artist_id == artist_id)
        return query.first()

    @staticmethod
    def get_one(entity_id: int):
        """
        get a single entity instance, return None if not found
        """
        try:
            return Release[entity_id]
        except orm.ObjectNotFound:
            return None


class Track(dopg.database.Entity):
    """
    track Entity for PonyORM
    """

    _table_ = "tracks"

    track_id = orm.PrimaryKey(int, auto=True, column="id")
    name = orm.Required(str)
    release = orm.Required(Release, column="release_id")
    location = orm.Optional(str)
    discogs_extra = orm.Optional(orm.Json)
    position = orm.Optional(str)
    users = orm.Set("User")  # one half of the user_tracks relationship

    def __repr__(self):
        tid = f"track_id={self.track_id}"
        n = f"name={self.name}"
        r = f"release={self.release}"
        l = f"location={self.location}"
        p = f"position={self.position}"
        return f"Track({tid}, {n}, {r}, {l}, {p})"

    @staticmethod
    def get_by_name(name: str, release_id: int = None):
        """
        get a track by name and, possibly, release name as well
        """
        query = Track.select(lambda t: _name_matches(t, name))
        if release_id is not None:
            query = query.filter(lambda t: t.release.release_id == release_id)
        return query.first()

    @staticmethod
    def get_one(entity_id: int):
        """
        get a single entity instance, return None if not found
        """
        try:
            return Track[entity_id]
        except orm.ObjectNotFound:
            return None


class User(dopg.database.Entity, UserMixin):
    """
    application user class
    """

    id = orm.PrimaryKey(int, auto=True, column="id")
    email = orm.Required(str, unique=True)
    password = orm.Required(str)
    name = orm.Required(str)
    last_login = orm.Optional(datetime)
    tracks = orm.Set(Track)

    @staticmethod
    @orm.db_session
    def get(user_id: int):
        """
        return an optional[User] instead of raising an exception
        """
        try:
            return User[user_id]
        except orm.ObjectNotFound:
            return None

    @staticmethod
    @orm.db_session
    def get_by_email(email: str):
        """
        return an optional[User] given an email address
        """
        return User.select(lambda u: u.email.lower() == email.lower()).first()


dopg.generate_mapping()
