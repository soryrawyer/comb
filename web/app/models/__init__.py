"""
Set dataclasses as top-level imports
"""

from .models import Artist, Release, Track, User

__all__ = ["Artist", "Release", "Track", "User"]
