"""
things related to the discogs client
"""

import logging
import os
import time

from dataclasses import dataclass
from typing import List, Optional, Tuple

import discogs_client
from discogs_client.models import Artist, Release, Track
from discogs_client.exceptions import HTTPError

from . import constants as c
from .config import Config
from .levenshtein import levenshtein_ratio


logger = logging.getLogger(__name__)


LEVENSHTEIN_THRESHOLD = 0.9


FIELD_MAPPING = {
    c.ARTIST: {c.NAME: "name", c.DISCOGS_ID: "id", c.DISCOGS_EXTRA: "data"},
    c.RELEASE: {c.NAME: "title", c.DISCOGS_ID: "id", c.DISCOGS_EXTRA: "data"},
    c.TRACK: {c.NAME: "title", c.POSITION: "position", c.DISCOGS_EXTRA: "data"},
}


def get_client(token=None) -> discogs_client.Client:
    """
    return a discogs_client.Client object
    """
    if not token:
        token = os.environ.get("DISCOGS_TOKEN")
    return discogs_client.Client("MyApp/0.1.0", user_token=token)


def get_levenshtein_ratios(name, tracklist: List[Track]):
    """
    given a track name and tracklist, return a list of tuples containing the
    levenshtein distances and track object
    """

    def prep_title(title):
        return title.lower().replace(" ", "")

    lower = prep_title(name)

    return [(levenshtein_ratio(lower, prep_title(t.title)), t) for t in tracklist]


def names_match(n1, n2):
    """
    compare the two names, and return whether the levenshtein distance is above the
    threshold
    """
    return levenshtein_ratio(n1.lower(), n2.lower()) >= LEVENSHTEIN_THRESHOLD


@dataclass
class DiscogsClient:
    """
    DiscogsClient: some common functions that might be helpful

    this class contains the conversion logic from discogs object to comb objects
    """

    _client: discogs_client.Client

    def make_api_call(self, func, *args, **kwargs):
        """
        this is kind of dumb and hacky, but basically we handle a bunch of
        different API call failure modes here: rate limiting, timeouts, 50Xs, etc

        also, I don't think the discogs client library passes header information
        in responses, so text matching might be our best bet for handling these
        errors. idk. let's just hope for the best I guess
        """
        while True:
            try:
                resp = func(*args, **kwargs)
                return resp
            except HTTPError as err:
                if "you are making requests too quickly" in err.msg.lower():
                    logger.info("rate limited by discogs. sleeping for 60s")
                    time.sleep(60)
                    continue

    def get_artist(self, artist: str):
        """
        return the first result for this artist name
        """
        artist = self.make_api_call(self._client.search, artist, type="artist")
        if not artist:
            return None
        return artist[0]

    def get_release(self, release: str, artist: str = None) -> Release:
        """
        return the first result for this artist + release
        """
        search_term = release
        if artist is not None:
            search_term = f"{artist} {release}"
        resp = self.make_api_call(self._client.search, search_term, type="master")
        if not resp:
            return None
        rel = resp[0]
        rel.tracklist
        if not names_match(rel.title, release):
            return None

        return rel

    def get_info_from_track(
        self, track_name: str, release_name: str, artist_name: str
    ) -> Tuple[Optional[Artist], Optional[Release], Optional[Track]]:
        """
        given the track name, release name, and artist name, return
        objects for all three.
        It's convenient to return all three at the same time, because
        we need to go through artists and releases to get to a track
        object using Discogs' API
        """
        query = f"{artist_name} {release_name}"
        resp = self.make_api_call(self._client.search, query, type="master")
        track = None
        artist = None
        release = None
        for release in resp:
            release.tracklist
            if not names_match(release.title, release_name):
                continue

            ratios = get_levenshtein_ratios(track_name, release.tracklist)
            sorted_ratios = sorted(ratios, key=lambda x: x[0], reverse=True)
            if sorted_ratios and sorted_ratios[0][0] < LEVENSHTEIN_THRESHOLD:
                continue

            track = sorted_ratios[0][1]

            d_artist = [
                a
                for a in release.main_release.artists
                if names_match(artist_name, a.name)
            ]
            if not d_artist:
                # we might want to raise an exception here. I think this branch
                # represents the following case:
                # we found a release with a matching name and a track with a matching name,
                # but the artist name didn't match any of the attributed artists for the release
                # maybe we want to keep going, instead?
                return None, None, None
            artist = d_artist[0]

            return (artist, release, track)

        # we should try to look up an artist and/or release, even
        # if we didn't find the track
        if track is None:
            artist = self.get_artist(artist_name)
            release = self.get_release(release_name, artist_name)
            # ok, this is kinda strange, but something about accessing the "tracklist"
            # attribute results in the title of the release being truncated to just the
            # release title. before accessing "tracklist", the title is "<artist> - <release>"
            if release is not None:
                release.tracklist
            return (artist, release, track)

        return (None, None, None)

    @classmethod
    def from_env(cls):
        """
        return a discogs client based on DISCOGS_TOKEN environment variable
        """
        return cls(get_client())

    @classmethod
    def from_config(cls, config: Config = None):
        """
        return a discogs client based on the application configuration
        """
        if config is None:
            config = Config()

        token = config.get("DISCOGS_TOKEN")
        client = discogs_client.Client("MyApp/0.1.0", user_token=token)
        return cls(client)
