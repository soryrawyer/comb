class Comb {
    constructor(artists, releases, tracks) {
        this.artists = artists;
        this.releases = releases;
        this.tracks = tracks;
    }

    getSelectedClass(className) {
        let selected = document.getElementsByClassName(`is-selected ${className}`);
        if (selected.length == 0) {
            return undefined;
        }
        return selected[0].textContent;
    }

    get selectedArtist() {
        let artistName = this.getSelectedClass("artist-name");
        for (let i in this.artists) {
            if (this.artists[i].name == artistName) {
                return this.artists[i];
            }
        }
    }

    get selectedRelease() {
        let releaseName = this.getSelectedClass("release-name");
        for (let i in this.releases) {
            if (this.releases[i].name == releaseName) {
                return this.releases[i];
            }
        }
    }

    get selectedTrack() {
        let trackName = this.getSelectedClass("track-name");
        for (let i in this.tracks) {
            if (this.tracks[i].name == trackName) {
                return this.tracks[i];
            }
        }
    }

    /**
     * remove the is-selected class from whatever has it for className
     * add the is-selected class to the new element
     * @param {*} name
     * @param {*} className 
     * @param {*} element 
     */
    updateSelected(element, className) {
        let old = document.getElementsByClassName(`${className} is-selected`)[0];
        old.classList.remove("is-selected");
        element.classList.add("is-selected");
    }

    drawTable() {
        let isSelected = function(name, selectedEntity, index) {
            if (selectedEntity === undefined) {
                return index == 0;
            } else {
                return name === selectedEntity.name
            }
        };

        let defaultEmpty = function(value) {
            if (value === undefined) {
                return "";
            }
            return value.name;
        };

        // TODO: preserve the "is-selected" class attributes for certain cells
        let newTableBody = document.createElement("tbody");
        let maxLength = Math.max(...[this.artists.length, this.releases.length, this.tracks.length]);
        for (let i = 0; i < maxLength; i++) {
            let artist = defaultEmpty(this.artists[i]);
            let artistSelected = isSelected(artist, this.selectedArtist, i);
            let release = defaultEmpty(this.releases[i]);
            let releaseSelected = isSelected(release, this.selectedRelease, i);
            let track = defaultEmpty(this.tracks[i]);
            let trackSelected = isSelected(track, this.selectedTrack, i);
            let row = this.tableRowHtml(artist, artistSelected, release, releaseSelected, track, trackSelected);
            newTableBody.appendChild(row);
        }
        // ok, now we have the full table. let's... do something
        let table = document.getElementById("track-table")
        let oldTableBody = document.getElementsByTagName("tbody")[0];
        table.removeChild(oldTableBody);
        table.appendChild(newTableBody);

        // we need to re-initialize all the event handlers too!
        // there's probably a better way to handle this
        initHandlers();
    }

    tableRowCellHtml(name, type, selected) {
        let el = document.createElement("td");
        el.textContent = name;
        let classNames = `${type}-name`;
        if (selected) {
            classNames += " is-selected"
        }
        el.setAttribute("class", classNames);
        return el;
    }

    tableRowHtml(artist, artistSelected, release, releaseSelected, track, trackSelected) {
        // each table row is going to have three cells:
        // artist name, with class "artist-name"
        // release name, with class "release-name"
        // track name, with class "track-name"
        let row = document.createElement("tr");
        row.appendChild(this.tableRowCellHtml(artist, "artist", artistSelected));
        row.appendChild(this.tableRowCellHtml(release, "release", releaseSelected));
        row.appendChild(this.tableRowCellHtml(track, "track", trackSelected));
        return row;
    }

    constructQuery(queries) {
        let query = "";
        for (let key in queries) {
            if (key === undefined || queries[key] === undefined) {
                continue;
            }

            query += `${key}=${queries[key]}`;
        }
        return query;
    }

    updateFromApi(collection, queries) {
        let query = `?${this.constructQuery(queries)}`;
        let url = `/${collection}${query}`
        fetch(url).then(response => response.json())
        .then(response => this[collection] = response)
        .then(_ => this.drawTable());
    }

    updateArtists() {
        this.updateFromApi("artists");
    }

    updateReleases() {
        let artist = this.selectedArtist;
        if (artist !== undefined) {
            this.updateFromApi("releases", {artist_id: artist.artist_id});
        }
    }

    updateTracks() {
        let release = this.selectedRelease;
        if (release !== undefined) {
            this.updateFromApi("tracks", {release_id: release.release_id});
        }
    }
}

function ahhh() {
    console.log("AHHHHHHH!!!")
}

function initComb(obj) {
    fetch("/artists").then(response => response.json()).then(function (artistJson) {
        obj.artists = artistJson;
    }).then(function (response) {
        let artist = obj.selectedArtist;
        let query = obj.constructQuery({artist_id: artist.artist_id})
        let url = `/releases?${query}`
        fetch(url).then(response => response.json()).then(function (releaseJson) {
            obj.releases = releaseJson;
        }).then(function (response) {
            let release = obj.selectedRelease;
            let query = obj.constructQuery({release_id: release.release_id});
            let url = `/tracks?${query}`
            fetch(url).then(response => response.json()).then(function (trackJson) {
                obj.tracks = trackJson;
            })
        })
    })
}

const comb = new Comb([], [], []);
initComb(comb);

// event handler-related stuff

function tableClicked(element) {
    // check to see if the element was an artist or a release
    // and update the table accordingly
    // maybe, check if "artist-name" or "release-name" is in this.classList
    if (this.classList.contains("artist-name")) {
        // we've got an artist click! update releases and tracks
        if (this.textContent == "" || this.textContent === undefined) {
            return;
        }
        comb.updateSelected(this, "artist-name");
        comb.updateReleases();
        // TODO: figure out promises so I can chain the update calls together
        // comb.updateTracks();
    } else if (this.classList.contains("release-name")) {
        if (this.textContent == "" || this.textContent === undefined) {
            return;
        }
        comb.updateSelected(this, "release-name");
        comb.updateTracks();
    }
}

function updateAudioHeader() {
    let artistDiv = document.getElementById("audio-artist");
    artistDiv.textContent = comb.selectedArtist.name;
    let releaseDiv = document.getElementById("audio-release");
    releaseDiv.textContent = comb.selectedRelease.name;
    let trackDiv = document.getElementById("audio-track");
    trackDiv.textContent = comb.selectedTrack.name;
}

function setAudio(element) {
    // get a signed URL from the backend and set that as the audio element's "src"
    comb.updateSelected(this, "track-name");
    let track = comb.selectedTrack;
    let url = `/playback_url/${track.track_id}`;
    fetch(url).then(response => response.json()).then(function (json) {
        let signedUrl = json["signed_url"];
        let audio = document.getElementsByTagName("audio")[0];
        audio.setAttribute("src", signedUrl);
        audio.setAttribute("type", "audio/mpeg");
        updateAudioHeader();
    })
}

function initBurgerHandler() {
    let burger = document.getElementsByClassName("navbar-burger")[0];
    burger.addEventListener("click", event => {
        let targetId = burger.dataset.target;
        let target = document.getElementById(targetId);
        burger.classList.toggle("is-active");
        target.classList.toggle("is-active");
    }, false);
}

function initHandlers() {
    let artistCells = document.getElementsByClassName("artist-name");
    for (let aCell of artistCells) {
        if (aCell.textContent == "" || aCell.textContent == undefined) {
            continue;
        }
        aCell.addEventListener("click", tableClicked, false);
    }

    let releaseCells = document.getElementsByClassName("release-name");
    for (let rCell of releaseCells) {
        if (rCell.textContent == "" || rCell.textContent == undefined) {
            continue;
        }
        rCell.addEventListener("click", tableClicked, false);
    }

    let trackCells = document.getElementsByClassName("track-name");
    for (let tCell of trackCells) {
        if (tCell.textContent == "" || tCell.textContent == undefined) {
            continue;
        }
        tCell.addEventListener("dblclick", setAudio, false);
    }

    initBurgerHandler();
}

initHandlers();
