"""
file storage (gcs)-related operations
"""

from flask import json, Blueprint
from flask_login import login_required

from app.models import Track
from app.storage import GcsStorage

gcs = GcsStorage.from_config()

blueprint = Blueprint("storage", __name__)


@blueprint.route("/playback_url/<int:track_id>")
@login_required
def get_signed_url(track_id):
    track = Track[track_id]
    resp = {"signed_url": gcs.get_signed_url(track)}
    return json.jsonify(resp)


@blueprint.route("/upload_url/<int:track_id>")
@login_required
def get_signed_upload_url(track_id):
    track = Track[track_id]
    resp = {"signed_url": gcs.get_signed_upload_url(track)}
    return json.jsonify(resp)
