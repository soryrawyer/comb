"""
entity routes (artist, release, track)
"""

from flask import json, request, Blueprint
from flask_login import current_user, login_required
from pony import orm

from app.config import Config
from app.discogs import DiscogsClient
from app.models import Artist, Release, Track
from app.storage import GcsStorage


blueprint = Blueprint("entities", __name__)

config = Config()
discogs_client = DiscogsClient.from_config(config)
gcs = GcsStorage.from_config(config)


@blueprint.route("/artists/")
@blueprint.route("/artists/<int:artist_id>")
@login_required
def get_artists(artist_id: int = None):
    # TODO: let this also take an artist name as a query parameter, and return
    # an error
    artists = orm.select(a for a in Artist)
    if artist_id is not None:
        artists = artists.filter(lambda a: a.artist_id == artist_id)
    return json.jsonify([a.to_dict() for a in artists.sort_by(Artist.name)])


@blueprint.route("/artists", methods=["POST"])
@login_required
def create_artist():
    # uhhh, couple steps here:
    # 1. check to see if the artist already exists?
    #    seems kind of difficult to do without more context. like, say, a release name
    name = request.json["name"]
    artist = Artist.get_by_name(name)
    if not artist:
        # doesn't exist, let's make an artist
        d_artist = discogs_client.get_artist(name)
        if d_artist is None:
            artist = Artist(name=name)
        else:
            artist = Artist.get_by_name(d_artist.name)
            if artist is None:
                artist = Artist(name=d_artist.name)

    orm.commit()
    return json.jsonify({"artist_name": artist.name, "artist_id": artist.artist_id})


@blueprint.route("/releases/")
@login_required
def get_releases():
    artist_id = request.args.get("artist_id")
    releases = orm.select(r for r in Release)
    if artist_id is not None:
        releases = releases.filter(lambda r: r.artist.artist_id == artist_id)
    return json.jsonify([r.to_dict() for r in releases.sort_by(Release.name)])


@blueprint.route("/releases/<int:release_id>")
@login_required
def get_one_release(release_id):
    return json.jsonify(Release[release_id].to_dict())


@blueprint.route("/releases", methods=["POST"])
@login_required
def create_release():
    """
    create a new release, or return an existing release if there already exists
    a release with the given name for the given artist

    the artist must already exist in the application
    """
    name = request.json["name"]
    artist_id = request.json["artist_id"]
    artist = Artist.get_one(artist_id)
    if artist is None:
        return json.jsonify({"msg": "must provide a valid artist"}), 400

    release = Release.get_by_name(name, artist_id=artist.artist_id)
    if release is None:
        # ok, no release by the name given, maybe discogs will have something?
        d_release = discogs_client.get_release(name)
        if d_release is None:
            release = Release(name=name, artist=artist)
        else:
            release = Release.get_by_name(d_release.title, artist_id=artist.artist_id)
            if not release:
                # doesn't exist, let's make a release
                release = Release(name=d_release.title, artist=artist)

    orm.commit()
    return json.jsonify(
        {"release_name": release.name, "release_id": release.release_id}
    )


@blueprint.route("/tracks/")
@login_required
def get_tracks():
    release_id = request.args.get("release_id")
    tracks = orm.select(t for t in Track)
    if release_id is not None:
        tracks = tracks.filter(lambda t: t.release.release_id == release_id)
    tracks = tracks.sort_by(Track.position)
    return json.jsonify([t.to_dict() for t in tracks])


@blueprint.route("/tracks/<int:track_id>")
@login_required
def get_one_track(track_id):
    return json.jsonify(Track[track_id].to_dict())


@blueprint.route("/tracks", methods=["POST"])
@login_required
def create_track():
    """
    given a track name and a release ID, either return the track if it exists,
    or create a new Track

    to create a new Track:
    1. get the Release associated with the given release ID, return error if
       release is not found
    2. look up track by name
    3. if track not found, call Discogs to get track, release, artist information
    4. look up track by Discogs name
    5. if track not found, create a new Track with the discogs name and release
    """
    release_id = request.json["release_id"]
    release = Release[release_id]
    if not release:
        return json.jsonify({"msg": "must provide a valid release"}), 400

    name = request.json["name"]
    track = Track.get_by_name(name, release_id=release_id)
    if not track:
        # doesn't exist, let's make an track
        (_, _, d_track) = discogs_client.get_info_from_track(
            name, release.name, release.artist.name
        )
        if d_track is None:
            position = request.json.get("position")
            if position is None:
                position = "0"
            # we don't have a track position, so just fill it in to 0
            track = Track(name=name, release=release, position=position)
        else:
            track = Track.get_by_name(d_track.title, release.release_id)
            if not track:
                track = Track(
                    name=d_track.title,
                    release=release,
                    position=d_track.position,
                )

    current_user.tracks.add(track)

    orm.commit()
    return json.jsonify({"track_name": track.name, "track_id": track.track_id})


@blueprint.route("/track/<int:track_id>/upload_url")
@login_required
def get_track_upload_link(track_id):
    track = Track.get_one(track_id)
    if track is None:
        return json.jsonify({"msg": "must provide a valid track ID"}), 400

    suffix = request.args.get("suffix")
    return json.jsonify({"signed_url": gcs.get_signed_upload_url(track, suffix=suffix)})
