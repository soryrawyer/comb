"""
routes, things like that
"""

from datetime import datetime
from itertools import zip_longest

from flask import Blueprint, render_template, json
from flask_login import current_user, login_required
from pony import orm

from app.models import Artist, Release, Track


main = Blueprint("main", __name__)


@main.route("/")
def index():
    return render_template("index.html")


@main.route("/ping")
def ping():
    return json.jsonify({"pong": datetime.now()})


@main.route("/profile")
@login_required
def profile():
    artists = orm.select(a for a in Artist).sort_by(Artist.name)
    selected_artist = artists.first()
    releases = orm.select(r for r in Release if r.artist == selected_artist).sort_by(
        Release.name
    )
    selected_release = releases.first()
    tracks = orm.select(t for t in Track if t.release == selected_release)
    lines = zip_longest(artists.fetch(), releases.fetch(), tracks.fetch())
    return render_template("profile.html", name=current_user.name, lines=lines)
