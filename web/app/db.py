"""
horsin around
"""

from typing import Optional

from pony import orm

from .config import Config


class Dopg:
    __db: Optional[orm.Database] = None

    def __new__(cls, host, user, password, port, db_name):
        if cls.__db is not None:
            return super(Dopg, cls).__new__(cls)

        cls.__db = orm.Database()
        cls.__db.bind(
            provider="postgres",
            user=user,
            password=password,
            host=host,
            port=port,
            database=db_name,
        )
        return super(Dopg, cls).__new__(cls)

    @property
    def database(self):
        return self.__db

    def generate_mapping(self):
        """
        generate a mapping for the bound database
        """
        try:
            self.__db.generate_mapping(create_tables=True)
        except orm.BindingError:
            # beside calling generate_mapping twice, it's unclear to me what
            # other actions might result in a "BindingError"
            pass

    @classmethod
    def from_config(cls, config: Config = None):
        """
        return a DB instance given a Config instance
        """
        if not config:
            config = Config()

        host = config.get("PGHOST")
        user = config.get("PGUSER")
        password = config.get("PG_PASSWD")
        port = config.get("PGPORT")
        db_name = config.get("PGDATABASE")
        return cls(host, user, password, port, db_name)


dopg = Dopg.from_config()
