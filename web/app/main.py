"""
some flask stuff

example command:
FLASK_APP=comb.app.main:app flask run
"""

from flask import Flask
from flask_login import LoginManager
from pony.flask import Pony

from .models import User
from .routes import main as main_blueprint
from .routes.entities import blueprint as entities_blueprint
from .routes.storage import blueprint as storage_blueprint
from .auth import auth as auth_blueprint

app = Flask(__name__)
app.config.update(
    {
        "DEBUG": False,
        "SECRET_KEY": "AHHHHH",
        "PONY": {"provider": "postgres"},
    }
)

app.register_blueprint(main_blueprint)
app.register_blueprint(auth_blueprint)
app.register_blueprint(entities_blueprint)
app.register_blueprint(storage_blueprint)
Pony(app)

login_manager = LoginManager(app)
login_manager.login_view = "auth.login"
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id: int):
    return User[user_id]
