"""
compute the levenshtein distance between two strings
"""

import copy


def init_2d(m, n):
    columns = [0] * m
    rows = [0] * n
    for idx, _ in enumerate(columns):
        columns[idx] = copy.deepcopy(rows)
    return columns


def levenshtein(base, comp):
    """
    iterative levenshtein distance calculation
    """
    m = len(base)
    n = len(comp)
    if min(m, n) == 0:
        return max(m, n)

    d = init_2d(m + 1, n + 1)
    for i in range(1, m + 1):
        d[i][0] = i

    for j in range(1, n + 1):
        d[0][j] = j

    for j in range(1, n + 1):
        for i in range(1, m + 1):
            if base[i - 1] == comp[j - 1]:
                substitution_cost = 0
            else:
                substitution_cost = 1

            d[i][j] = min(
                (
                    d[i - 1][j] + 1,
                    d[i][j - 1] + 1,
                    d[i - 1][j - 1] + substitution_cost,
                )
            )

    return d[m][n]


def levenshtein_ratio(base, comp):
    """
    return the levenshtein difference as a percentage difference
    """
    lev = levenshtein(base, comp)
    l = len(base)
    return (l - lev) / l
