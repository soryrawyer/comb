.PHONY: run install test docker-test

run:
	python -m comb.cli

install:
	python3 setup.py install

test:
	python3 -m pytest

docker-test:
	docker build --target test -t comb-test .
	docker run comb-test:latest pytest
