comb.cli package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   comb.cli.commands

Submodules
----------

comb.cli.audio\_file module
---------------------------

.. automodule:: comb.cli.audio_file
   :members:
   :undoc-members:
   :show-inheritance:

comb.cli.client module
----------------------

.. automodule:: comb.cli.client
   :members:
   :undoc-members:
   :show-inheritance:

comb.cli.main module
--------------------

.. automodule:: comb.cli.main
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: comb.cli
   :members:
   :undoc-members:
   :show-inheritance:
