updating metadata
given a filename:
get an object that has the metadata accessible
try to load track from dopg
AudioFile -> get a model for each part of a thing (artist, track, release)

for each part of the thing:

- get the discogs response for the part
- do any kind of updates
- write part back to db

after that, upload to GCS! then update the track!

ultimately:

- write to GCS
- store in db
  - artist
  - track
  - release

todo:

- write methods to pull from DB then overlay discogs data on top of it in metadata.py
- add a `from_discogs_object` to the Track class
- write a Release class
