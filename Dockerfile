FROM python:3.7-buster as base

RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt update

RUN apt install -y postgresql-client-11

RUN pip install pipenv

COPY . /opt/comb
WORKDIR /opt/comb
RUN pipenv install --system

FROM base as test
RUN pipenv install --dev --system
