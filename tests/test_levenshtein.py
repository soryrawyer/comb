"""
test the levenshtein distance func
"""

from comb.levenshtein import levenshtein

def test_empty_string():
    assert levenshtein("", "") == 0
    assert levenshtein("", "arrakis") == 7
    assert levenshtein("spice", "") == 5

def test_different():
    assert levenshtein("spice", "arrakis") == 7

def test_similar():
    assert levenshtein("spice rmx", "spice remix") == 2
