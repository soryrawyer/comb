"""
some simple tests for our flask application
"""

import os
import tempfile

import pytest

# from comb.app.main import app


# @pytest.fixture
# def client():
#     # TODO: this was pulled from the flask testing tutorial.
#     # I think we should try to replace the PONY section of the app config
#     # and use sqlite for testing
#     db_fd, app.config["DATABASE"] = tempfile.mkstemp()
#     app.config["TESTING"] = True

#     with app.test_client() as client:
#         yield client

#     os.close(db_fd)
#     os.unlink(app.config["DATABASE"])


def tk_test_empty_db(client):
    """
    example of how we could use the client fixture
    the 'tk_' prefix hides this function from pytest
    """
    resp = client.get("/artists")
    assert resp.data == []
